#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <wordexp.h>
#include <signal.h>
#include <sys/types.h>
#include <dirent.h>


#define PROMPT "# "
#define NELEMS(x)  (sizeof(x) / sizeof(x[0]))



const char* BUILT_IN[] = {"","exit","mycd","mypwd","setEnvVar","myls","concr"};

int cmd_prompt(char*, wordexp_t*);
void free_input(char*, wordexp_t*);
void exec_builtin(int, char*[]);
void exec_cmd(char*[]);
int is_builtin(char*[]);
void INThandler(int);
DIR *opendir(const char *name);
DIR *fdopendir(int fd);




/*
 * Function: main
 * --------------
 * Main command loop.
 */
int main(int argc, char* argv[])
{
    char* input_str = NULL;
    int cmd = 0;
    wordexp_t cmd_argv;

    signal(SIGINT, INThandler);
    while(1)
    {
        if(!cmd_prompt(input_str, &cmd_argv))
        {
            free_input(input_str, &cmd_argv);
            continue;
        }

        if ((cmd = is_builtin(cmd_argv.we_wordv)))
            exec_builtin(cmd, cmd_argv.we_wordv);   // exec built_in command
        else
            exec_cmd(cmd_argv.we_wordv);            // exec command

        free_input(input_str, &cmd_argv);
    }
}

/*
 * Function:  cmd_prompt
 * -----------------------
 * Handles command line input using readline and wordexp.
 * Prints the prompt and waits for user's input returning 0 if no command was
 * issued, or 1 otherwise with cmd_argv holding the parsed command line.
 */
int cmd_prompt(char* input_str, wordexp_t* cmd_argv)
{
    if(!(input_str = readline(PROMPT)))     // command input
    {
        printf("exit\n");
        exit(0);
    }

    int error = wordexp(input_str, cmd_argv, WRDE_SHOWERR | WRDE_UNDEF);

    if(error)
    {
        switch (error) {
            case WRDE_BADCHAR:
                printf("Bad character. Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }\n");
                break;
            case WRDE_BADVAL:
                printf("Undefined variable\n");
                break;
            case WRDE_SYNTAX:
                printf("Syntax error\n");
                break;
            default:
                printf("Unknown error\n");
        }

         return 0;
    }

    if (cmd_argv->we_wordc == 0) // no input
    {
        return 0;
    }

    add_history(input_str);

    return 1;
}

/*
 * Function: exec_cmd
 * ------------------
 * Execute given command and waits till it's completed. cmd_argv[0] must shields
 * the command to be executed, cmd_argv[i] (i > 0) shields the command arguments.
 */
void exec_cmd(char* cmd_argv[])
{
    int status;
    int child_pid = fork();

    if (child_pid != 0) // parent process
    {
        waitpid(child_pid, &status, 0);
    }
    else // child process
    {
        execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
        perror("");
        exit(0);
    }
}


/*
 * Function: fill
 * ----------------------
 * fills array1 with the cmd_argv elements, from 1, to size of argv.
 */
void fill(char* cmd2_argv[],char* cmd_argv[]){
  for(int i = 0;i<sizeof(cmd2_argv);i++){
    cmd2_argv[i]=cmd_argv[i+1];
  }
}

/*
 * Function: exec_builtin
 * ----------------------
 * Executes built-in commmands registered in BUILT_IN array.
 */
void exec_builtin(int cmd, char* cmd_argv[])
{
    char path[255];
    DIR *pDir;
    struct dirent *pDirent;

    switch(cmd)
    {
        case 1:
            exit(0);
            break;
        case 2:
            chdir(cmd_argv[1]);
            break;
        case 3:
            getcwd(path, 255);
            printf("%s\n", path);
            break;
        case 4:
            setenv(cmd_argv[1],cmd_argv[2],1);
            break;
        case 5:
            if(NELEMS(cmd_argv)<2){
              pDir=opendir(".");
            }else{
              pDir=opendir(cmd_argv[1]);
            }
            while ((pDirent = readdir(pDir)) != NULL) {
              printf ("[%s]\n", pDirent->d_name);
            }
            closedir(pDir);
            break;
        case 6:
            concrConcur(cmd_argv);
            break;
        default:
            printf("-bsh: command not found\n");
    }

    return;
}


void concrConcur(char* cmd_argv[]){
  int n = sizeof(cmd_argv)-1;
  char* cmd2_argv[n];
  int cmd2;
  fill(&cmd2_argv,cmd_argv);
  int status;
  int child_pid = fork();
  if (child_pid != 0) // parent process
  {
  }
  else // child process
  {
    if ((cmd2 = is_builtin(cmd2_argv)))
        exec_builtin(cmd2, cmd2_argv);   // exec built_in command
    else{
        execvp(cmd2_argv[0], cmd2_argv); // execute PROGRAM
        perror("");
        exit(0);
    }
  }

}

/*
 * Function: is_builtin
 * ----------------------
 * Finds out if the command is a built-in one, registered in BUILT_IN array and
 * returns the command position.
 */
int is_builtin(char* cmd_argv[])
{
    for(int i = 1; i < NELEMS(BUILT_IN); i++)
        if (!strcmp(cmd_argv[0], BUILT_IN[i]))
            return i;


    return 0;
}

/*
 * Function: INThandler
 * ----------------------
 * Handles the Ctrl-C
*/
void  INThandler(int sig){
  printf("dont touch ctr c");

}




// utility functions
void free_input(char* cmd_str, wordexp_t* cmd_argv)
{
    free(cmd_str);
    wordfree(cmd_argv);
}
